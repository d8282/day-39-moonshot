//
//  MoonshotApp.swift
//  Moonshot
//
//  Created by Drummer on 2022. 06. 05..
//

import SwiftUI

@main
struct MoonshotApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
